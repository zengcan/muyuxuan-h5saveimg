//保存canvas的图片到手机相册
var saveCanvas = function(CanvasId, callback) {
	uni.canvasToTempFilePath({
		canvasId: CanvasId,
		success(res) {
			var base64Img = res.tempFilePath;
			console.log('canvasToTempFilePath');
			saveBase64(base64Img, callback);
		}
	})

}
//保存base64的图片到手机相册
var saveBase64 = function(Base64Img, callback) {
	var bitmap = new plus.nativeObj.Bitmap("test");
	bitmap.loadBase64Data(Base64Img, function() {
		var url = "_doc/" + new Date().getTime() + ".png";
		bitmap.save(url, {
			overwrite: true
		}, function(e) {
			console.log('移动图片成功');
			console.log(url);
			plus.gallery.save(url, function() {
				if (callback) {
					callback(url)
				}
			}, function(e) {
				console.log('保存失败,位置保存图片');
				console.log(e);
			});
		}, function(e) {
			console.log('移动图片失败,位置保存到沙盒失败');
			console.log(e);
		})
	}, function(e) {
		console.log("图片保存失败,位置将base64转化成img");
		console.log(e);
	})
}
//保存网络的图片到手机相册
var savePath = function(ImgPath, callback) {
	var dtask = plus.downloader.createDownload(ImgPath, {}, function(d, status){
		console.log(d);
		console.log('下载完成');
			// 下载完成
			if(status == 200){ 
				plus.gallery.save(d.filename,function(){
					if(callback){
						callback(d.filename)
					}
				},function(e){
					console.log(e);
				})
				console.log("Download success: " + d.filename);
			} else {
				 console.log("下载失败");
			}  
		});
		//dtask.addEventListener("statechanged", onStateChanged, false);
		dtask.start(); 
}
module.exports = {
	saveCanvas: saveCanvas,
	saveBase64: saveBase64,
	savePath: savePath
}
