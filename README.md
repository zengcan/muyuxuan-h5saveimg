# wep2appsaveimg

#### 介绍
Wap2App网站下载图片到本地，包括Canvas，Base64，网络图片的保存，可在wap2app的网站中使用

#### 软件架构
软件架构说明


#### 安装教程
这个插件解决的是wap2app，指向h5网站，然后下载图片时，不能下载的问题
单纯h5直接长按保存，单纯app使用uni自带的下载保存，但wapapp，没有长按保存
也没有app下的方法，然后使用了H5+ 的plus实现


里面包括：

saveCanvas方法传入canvas的id，和回调
saveBase64方法传入base64字符串，和回调
savePath方法传入图片的url地址，和回调

